import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as imageService from 'src/services/imageService';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import ConfirmDelete from 'src/containers/ConfirmDelete';
import ShowErrorMessage from 'src/containers/ShowErrorMessage';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';

import {
  likePost,
  toggleExpandedPost,
  addComment,
  modalConfirm,
  closeConfirm,
  updatePost,
  deletePost,
  updateComment,
  deleteComment,
  showError,
  likeComment
} from 'src/containers/Thread/actions';

const ExpandedPost = ({
  modal,
  post,
  sharePost,
  userId,
  likePost: like,
  toggleExpandedPost: toggle,
  addComment: add,
  updatePost: updatePostAction,
  modalConfirm: modalConfirmAction,
  deletePost: deletePostAction,
  updateComment: updateCommentAction,
  deleteComment: deleteCommentAction,
  showError: showErrorAction,
  likeComment: likeCommentAction
}) => {
  const uploadImage = file => imageService.uploadImage(file);
  const updateImage = file => imageService.updateImage(file);
  const deleteImage = file => imageService.deleteImage(file);

  return (
    <Modal
      dimmer="blurring"
      centered={false}
      open
      onClose={() => toggle()}
      mountNode={document.getElementById('root')}
    >
      {post
        ? (
          <Modal.Content>
            <Post
              post={post}
              likePost={like}
              toggleExpandedPost={toggle}
              sharePost={sharePost}
              userId={userId}
              canEdit
              uploadImage={uploadImage}
              updateImage={updateImage}
              deleteImage={deleteImage}
              updatePost={updatePostAction}
              showError={showErrorAction}
              modalConfirm={modalConfirmAction}
              deletePost={deletePostAction}
            />
            <CommentUI.Group style={{ maxWidth: '100%' }}>
              <Header as="h3" dividing>
                Comments
              </Header>
              {post.comments && post.comments
                .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
                .map(comment => (
                  <Comment
                    key={comment.id}
                    comment={comment}
                    userId={userId}
                    postId={post.id}
                    updateComment={updateCommentAction}
                    showError={showErrorAction}
                    modalConfirm={modalConfirmAction}
                    deleteComment={deleteCommentAction}
                    likeComment={likeCommentAction}
                  />
                ))}
              <AddComment postId={post.id} addComment={add} showError={showErrorAction} />
            </CommentUI.Group>
          </Modal.Content>
        )
        : <Spinner />}
      {modal && <ConfirmDelete />}
      <ShowErrorMessage />
    </Modal>
  );
};

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
  updatePost: PropTypes.func.isRequired,
  modalConfirm: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  modal: PropTypes.bool,
  showError: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired
};

ExpandedPost.defaultProps = {
  modal: false
};

const mapStateToProps = rootState => ({
  post: rootState?.posts?.expandedPost,
  modal: rootState?.posts?.confirmDelete?.modal
});

const actions = {
  likePost,
  toggleExpandedPost,
  addComment,
  updatePost,
  modalConfirm,
  closeConfirm,
  deletePost,
  updateComment,
  deleteComment,
  showError,
  likeComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ExpandedPost);
