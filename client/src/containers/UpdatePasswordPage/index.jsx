import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import Logo from 'src/components/Logo';
import { Grid, Header } from 'semantic-ui-react';
import UpdatePasswordForm from 'src/components/UpdatePasswordForm';
import { updatePassword } from 'src/containers/Profile/actions';
import { showError } from 'src/containers/Thread/actions';

const UpdatePasswordPage = ({
  showError: showErrorAction,
  updatePassword: updatePasswordAction
}) => (
  <Grid textAlign="center" verticalAlign="middle" className="fill">
    <Grid.Column style={{ maxWidth: 450 }}>
      <Logo />
      <Header as="h2" color="teal" textAlign="center">
        New password to your account.
      </Header>
      <UpdatePasswordForm updatePassword={updatePasswordAction} showError={showErrorAction} />
    </Grid.Column>
  </Grid>
);

UpdatePasswordPage.propTypes = {
  updatePassword: PropTypes.func.isRequired,
  showError: PropTypes.func.isRequired
};

const actions = { updatePassword, showError };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(null, mapDispatchToProps)(UpdatePasswordPage);
