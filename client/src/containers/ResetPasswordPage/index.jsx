import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import Logo from 'src/components/Logo';
import { Grid, Header, Message } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import ResetPasswordForm from 'src/components/ResetPasswordForm';
import { showError } from 'src/containers/Thread/actions';
import { resetPassword } from 'src/containers/Profile/actions';

const ResetPasswordPage = ({
  showError: showErrorAction,
  resetPassword: resetPasswordAction
}) => (
  <>
    <Grid textAlign="center" verticalAlign="middle" className="fill">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Logo />
        <Header as="h2" color="teal" textAlign="center">
          Reset password in your account
        </Header>
        <ResetPasswordForm resetPassword={resetPasswordAction} showError={showErrorAction} />
        <Message>
          Did you remember your password?
          {' '}
          <NavLink exact to="/login">Sign In</NavLink>
        </Message>
      </Grid.Column>
    </Grid>
  </>
);

ResetPasswordPage.propTypes = {
  resetPassword: PropTypes.func.isRequired,
  showError: PropTypes.func.isRequired
};

const actions = { resetPassword, showError };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(null, mapDispatchToProps)(ResetPasswordPage);
