import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Confirm } from 'semantic-ui-react';

import { closeConfirm } from 'src/containers/Thread/actions';

const ConfirmDelete = ({
  deleteMessage,
  deleteFunction,
  deleteData,
  closeConfirm: close,
  backPath
}) => {
  const handlerDelete = async event => {
    event.preventDefault();
    await close(backPath);
    await deleteFunction(deleteData);
  };

  return (
    <Confirm
      open
      content={deleteMessage}
      onCancel={() => close(backPath)}
      onConfirm={handlerDelete}
    />
  );
};

ConfirmDelete.propTypes = {
  deleteMessage: PropTypes.string.isRequired,
  deleteFunction: PropTypes.func.isRequired,
  deleteData: PropTypes.any.isRequired, // eslint-disable-line
  closeConfirm: PropTypes.func.isRequired,
  backPath: PropTypes.string.isRequired
};

const mapStateToProps = ({ posts }) => ({
  deleteFunction: posts.confirmDelete.deleteFunction,
  deleteData: posts.confirmDelete.deleteData,
  deleteMessage: posts.confirmDelete.deleteMessage,
  backPath: posts.confirmDelete.backPath
});

const actions = { closeConfirm };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConfirmDelete);
