import { push } from 'connected-react-router';
import * as authService from 'src/services/authService';
import * as userService from 'src/services/userService';
import { SET_USER } from './actionTypes';

const setToken = token => localStorage.setItem('token', token);

const setUser = user => async dispatch => dispatch({
  type: SET_USER,
  user
});

const setAuthData = (user = null, token = '') => (dispatch, getRootState) => {
  setToken(token); // token should be set first before user
  setUser(user)(dispatch, getRootState);
};

const handleAuthResponse = authResponsePromise => async (dispatch, getRootState) => {
  const { user, token } = await authResponsePromise;
  setAuthData(user, token)(dispatch, getRootState);
};

export const login = request => handleAuthResponse(authService.login(request));

export const register = request => handleAuthResponse(authService.registration(request));

export const logout = () => setAuthData();

export const loadCurrentUser = () => async (dispatch, getRootState) => {
  const user = await authService.getCurrentUser();
  setUser(user)(dispatch, getRootState);
};

export const cancelEditProfileCurrentUser = () => dispatch => dispatch(push('/'));

export const updateProfile = ({ userId, ...profile }) => async dispatch => {
  const updated = await userService.updateProfile({ userId, ...profile });

  await dispatch(setUser(updated));
  await dispatch(push('/'));
};

export const resetPassword = async ({ email }) => {
  await authService.resetPassword({ email });
  window.location.replace(`https://${email}`);
};

export const updatePassword = ({ token: resetPasswordToken, password }) => async (dispatch, getRootState) => {
  setAuthData(null, resetPasswordToken)(dispatch, getRootState);
  const { user, token } = await authService.updatePassword({ password });
  setAuthData(user, token)(dispatch, getRootState);
};
