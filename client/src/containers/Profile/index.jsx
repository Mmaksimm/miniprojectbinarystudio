import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as imageService from 'src/services/imageService';
import ReactCrop from 'react-image-crop';
import { getUserImgLink } from 'src/helpers/imageHelper';
import ConfirmDelete from 'src/containers/ConfirmDelete';
import getCroppedImg from 'src/helpers/сroppedImgHelper';

import 'react-notifications/lib/notifications.css';
import 'react-image-crop/dist/ReactCrop.css';

import {
  Grid,
  Image,
  Input,
  Button
} from 'semantic-ui-react';

import {
  modalConfirm,
  closeConfirm,
  refreshAllPost,
  showError
} from 'src/containers/Thread/actions';

import { updateProfile } from './actions';

const Profile = ({
  modal,
  user,
  modalConfirm: modalConfirmAction,
  updateProfile: updateProfileAction,
  refreshAllPost: refreshAllPostAction,
  showError: showErrorAction
}) => {
  const { username, email, status, image, id } = user;
  const [getImage, setImage] = useState(null);
  const [getUsername, setUsername] = useState(username);
  const [getStatus, setStatus] = useState(status);
  const [isCutAvatar, setCutAvatar] = useState(false);
  const [getPhoto, setPhoto] = useState(image);
  const [crop, setCrop] = useState({
    aspect: 1 / 1,
    unit: 'px',
    maxWidth: 200,
    maxHeight: 200
  });

  const usernameChanged = ({ target: { value } }) => {
    setUsername(value);
  };

  const statusChanged = ({ target: { value } }) => {
    setStatus(value);
  };

  const handlePreviewPhoto = async ({ target }) => {
    try {
      const { files: [FileList] } = await target;
      if (FileList.type.slice(0, 5) !== 'image') return;
      const link = await URL.createObjectURL(FileList);
      await setPhoto({ link, file: FileList });
      setCutAvatar(true);
    } catch (err) {
      showErrorAction('Image preview error.');
    }
  };

  const handlePreviewDeletePhoto = async () => {
    try {
      await modalConfirmAction({
        deleteFunction: setPhoto,
        deleteData: false,
        backPath: '/profile',
        deleteMessage: 'Do you want to delete the photo?'
      });
    } catch (err) {
      showErrorAction('Image deletion error.');
    }
  };

  const handleCancelUpdatePhoto = () => {
    setPhoto(image);
    setCutAvatar(false);
  };

  const handleGetCropedImage = async () => {
    try {
      const { file: { name } } = getPhoto;
      const newAvatar = await getCroppedImg({ crop, image: getImage, name });
      setPhoto(newAvatar);
    } catch {
      setPhoto(image);
      showErrorAction('Avatar update error.');
    } finally {
      setCutAvatar(false);
    }
  };

  const handleDeletePhoto = async () => {
    await imageService.deleteImage(image.id);
  };

  const handleUploadPhoto = async file => {
    const { link, id: imageId } = await imageService.uploadImage(file);
    setImage({ link, id: imageId });
    return {
      link,
      id: imageId
    };
  };

  const handleUpdatePhoto = async file => {
    const { link, id: imageId } = await imageService.updateImage({
      image: file,
      imageId: image.id
    });
    setImage({ link, id: imageId });
    return {
      link,
      id: imageId
    };
  };

  const handleUpdateProfile = async () => {
    if (!getUsername || (getUsername === username && getStatus === status && getPhoto === image)) {
      setPhoto(image);
      setUsername(username);
      setStatus(status);
      return;
    }

    try {
      switch (getPhoto) {
        case image:
          await updateProfileAction({
            imageId: image.id,
            userId: id,
            username: getUsername,
            status: getStatus
          });
          break;

        case false:
          await handleDeletePhoto();
          await updateProfileAction({
            userId: id,
            username: getUsername,
            status: getStatus,
            image: {
              id: null,
              link: ''
            }
          });
          break;

        default:
          if (image && image.link) {
            const response = await handleUpdatePhoto(getPhoto.file);
            await updateProfileAction({
              imageId: response.id,
              userId: id,
              username: getUsername,
              status: getStatus
            });
          } else {
            const response = await handleUploadPhoto(getPhoto.file);
            await updateProfileAction({
              imageId: response.id,
              userId: id,
              username: getUsername
            });
          }
      }
      refreshAllPostAction({ userId: id, username: getUsername, status: getStatus });
    } catch (err) {
      setUsername(username);
      setImage(image);
      setStatus(status);
      // eslint-disable-next-line no-bitwise
      if (~err.message.indexOf('users_username_key')) {
        showErrorAction('Such the name already exists.');
      } else {
        showErrorAction('Profile update error.');
      }
    }
  };

  return (
    <>
      <Grid container textAlign="center" style={{ paddingTop: 30 }}>
        <Grid.Column>
          {isCutAvatar
            ? (
              <>
                <ReactCrop src={getPhoto.link} crop={crop} onChange={setCrop} onImageLoaded={setImage} />
                <br />
                <br />
                <Button.Group>
                  <Button
                    color="teal"
                    onClick={handleGetCropedImage}
                  >
                    Save
                  </Button>
                  <Button onClick={handleCancelUpdatePhoto}>Cancel</Button>
                </Button.Group>
              </>
            )
            : <Image centered src={getUserImgLink(getPhoto)} size="medium" circular />}
          <br />
          {!getPhoto && !isCutAvatar && (
            <Button color="teal" as="label">
              Attach
              <input name="image" type="file" onChange={handlePreviewPhoto} hidden />
            </Button>
          )}
          {getPhoto && !isCutAvatar && (
            <Button.Group>
              <Button color="teal" as="label">
                Change
                <input name="image" type="file" onChange={handlePreviewPhoto} hidden />
              </Button>
              <Button.Or />
              <Button negative onClick={handlePreviewDeletePhoto}>Delete</Button>
            </Button.Group>
          )}
          <br />
          <br />
          <Input
            icon="user"
            iconPosition="left"
            placeholder="Username"
            type="text"
            value={getUsername}
            onChange={usernameChanged}
          />
          <br />
          <br />
          <Input
            icon="at"
            iconPosition="left"
            placeholder="Email"
            type="email"
            disabled
            value={email}
          />
          <br />
          <br />
          <Input
            placeholder="Status"
            type="text"
            value={getStatus}
            onChange={statusChanged}
          />
          <br />
          <br />
          {!isCutAvatar && (
            <Button.Group>
              <Button
                color="teal"
                onClick={handleUpdateProfile}
              >
                Update
              </Button>
              <NavLink exact to="/">
                <Button>Cancel</Button>
              </NavLink>
            </Button.Group>
          )}
        </Grid.Column>
      </Grid>
      {modal && <ConfirmDelete />}
    </>
  );
};

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  modalConfirm: PropTypes.func.isRequired,
  modal: PropTypes.bool,
  updateProfile: PropTypes.func.isRequired,
  refreshAllPost: PropTypes.func.isRequired,
  showError: PropTypes.func.isRequired
};

Profile.defaultProps = {
  user: {},
  modal: false
};

const mapStateToProps = rootState => ({
  user: rootState?.profile?.user,
  modal: rootState?.posts?.confirmDelete?.modal,
  deleteFunction: rootState?.posts?.confirmDelete?.deleteFunction,
  deleteData: rootState?.posts?.confirmDelete?.deleteData,
  deleteMessage: rootState?.posts?.confirmDelete?.deleteMessage
});

const actions = {
  modalConfirm,
  closeConfirm,
  updateProfile,
  refreshAllPost,
  showError
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
