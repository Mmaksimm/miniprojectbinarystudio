import React, { useEffect } from 'react';
import { Route, Switch } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Thread from 'src/containers/Thread';
import LoginPage from 'src/containers/LoginPage';
import RegistrationPage from 'src/containers/RegistrationPage';
import Profile from 'src/containers/Profile';
import Header from 'src/components/Header';
import SharedPost from 'src/containers/SharedPost';
import Spinner from 'src/components/Spinner';
import NotFound from 'src/scenes/NotFound';
import PrivateRoute from 'src/containers/PrivateRoute';
import PublicRoute from 'src/containers/PublicRoute';
import Notifications from 'src/components/Notifications';
import ShowErrorMessage from 'src/containers/ShowErrorMessage';
import ResetPasswordPage from 'src/containers/ResetPasswordPage';
import UpdatePasswordPage from 'src/containers/UpdatePasswordPage';
import PropTypes from 'prop-types';

import 'react-notifications/lib/notifications.css';

import {
  loadCurrentUser,
  logout,
  updateProfile
} from 'src/containers/Profile/actions';

import {
  applyPost,
  refreshPost,
  removePost,
  applyComment,
  refreshComment,
  removeComment,
  refreshAllPost,
  showError
} from 'src/containers/Thread/actions';

const Routing = ({
  user,
  isAuthorized,
  isLoading,
  applyPost: newPost,
  refreshPost: updatePost,
  logout: signOut,
  loadCurrentUser: loadUser,
  removePost: deletePost,
  applyComment: addComment,
  refreshComment: updateComment,
  removeComment: deleteComment,
  refreshAllPost: updateAllPost,
  updateProfile: updateProfileAction,
  showError: showErrorAction
}) => {
  useEffect(() => {
    if (!isAuthorized) {
      loadUser();
    }
  });
  return (
    isLoading
      ? <Spinner />
      : (
        <div className="fill">
          {isAuthorized && (
            <header>
              <Header
                user={user}
                logout={signOut}
                updateProfile={updateProfileAction}
                refreshAllPost={updateAllPost}
                showError={showErrorAction}
              />
            </header>
          )}
          <main className="fill">
            <Switch>
              <PublicRoute exact path="/login" component={LoginPage} />
              <PublicRoute exact path="/registration" component={RegistrationPage} />
              <PublicRoute exact path="/reset-password" component={ResetPasswordPage} />
              <PublicRoute exact path="/update-password/:token" component={UpdatePasswordPage} />
              <PrivateRoute exact path="/" component={Thread} />
              <PrivateRoute exact path="/profile" component={Profile} />
              <PrivateRoute path="/share/:postHash" component={SharedPost} />
              <Route path="*" exact component={NotFound} />
            </Switch>
          </main>
          <Notifications
            applyPost={newPost}
            user={user}
            refreshPost={updatePost}
            removePost={deletePost}
            applyComment={addComment}
            refreshComment={updateComment}
            removeComment={deleteComment}
            refreshAllPost={updateAllPost}
          />
          <ShowErrorMessage />
        </div>
      )
  );
};

Routing.propTypes = {
  isAuthorized: PropTypes.bool,
  logout: PropTypes.func.isRequired,
  applyPost: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any),
  isLoading: PropTypes.bool,
  loadCurrentUser: PropTypes.func.isRequired,
  refreshPost: PropTypes.func.isRequired,
  removePost: PropTypes.func.isRequired,
  applyComment: PropTypes.func.isRequired,
  refreshComment: PropTypes.func.isRequired,
  removeComment: PropTypes.func.isRequired,
  refreshAllPost: PropTypes.func.isRequired,
  updateProfile: PropTypes.func.isRequired,
  showError: PropTypes.func.isRequired
};

Routing.defaultProps = {
  isAuthorized: false,
  user: {},
  isLoading: true
};

const actions = {
  loadCurrentUser,
  logout,
  applyPost,
  refreshPost,
  removePost,
  applyComment,
  refreshComment,
  removeComment,
  refreshAllPost,
  updateProfile,
  showError
};

const mapStateToProps = rootState => ({
  isAuthorized: rootState.profile.isAuthorized,
  user: rootState.profile.user,
  isLoading: rootState.profile.isLoading
});

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Routing);
