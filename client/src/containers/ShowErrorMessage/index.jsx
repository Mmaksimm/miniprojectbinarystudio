import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { NotificationContainer, NotificationManager } from 'react-notifications';

import 'react-notifications/lib/notifications.css';

const ShowErrorMessage = ({ errorMessage }) => {
  if (errorMessage) {
    NotificationManager.error(errorMessage);
  }
  return <NotificationContainer />;
};

ShowErrorMessage.propTypes = {
  errorMessage: PropTypes.string
};

ShowErrorMessage.defaultProps = {
  errorMessage: ''
};

const mapStateToProps = rootState => ({
  errorMessage: rootState.posts.errorMessage
});

export default connect(mapStateToProps)(ShowErrorMessage);
