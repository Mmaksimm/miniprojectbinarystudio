import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST,
  SET_MODAL_CONFIRM,
  CLOSE_CONFIRM,
  SHOW_ERROR
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const newPost = await postService.addPost(post);
  dispatch(addPostAction(newPost));
};

export const refreshPost = postId => async (dispatch, getRootState) => {
  const updatedPost = await postService.getPost(postId);

  const { posts: { posts, expandedPost } } = getRootState();

  const updated = posts.map(post => (post.id !== postId ? post : updatedPost));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(updatedPost));
  }
};

export const updatePost = ({ postId, body, image }) => async (dispatch, getRootState) => {
  const { updatedAt } = await postService.updatePost({ postId, body, imageId: image.id });

  const { posts: { posts, expandedPost } } = getRootState();
  const mapPosts = post => ({
    ...post,
    body,
    updatedAt,
    image: {
      ...post.image,
      ...image
    }
  });

  const updated = posts.map(post => (post.id !== postId ? post : mapPosts(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapPosts(expandedPost)));
  }
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const likePost = (postId, user, isLike) => async (dispatch, getRootState) => {
  const { diffDislike, postReactions } = await postService.likePost(postId, user, isLike);

  const mapLikes = post => ({
    ...post,
    postReactions,
    dislikeCount: Number(post.dislikeCount) + diffDislike
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const comment = await commentService.addComment(request);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const modalConfirm = deleted => ({
  type: SET_MODAL_CONFIRM,
  payload: deleted
});

export const closeConfirm = () => ({
  type: CLOSE_CONFIRM
});

export const deletePost = ({ postId }) => async (dispatch, getRootState) => {
  await postService.deletePost(postId);

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.filter(post => (post.id !== postId));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(false));
  }
};

export const removePost = postId => (dispatch, getRootState) => {
  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.filter(post => (post.id !== postId));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(false));
  }
};

export const updateComment = ({ commentId, postId, body }) => async (dispatch, getRootState) => {
  const { updatedAt } = await commentService.updateComment({ commentId, body });

  const mapComments = comment => ({
    ...comment,
    body,
    updatedAt
  });

  const mapPost = post => ({
    ...post,
    comments: post.comments.map(comment => (
      comment.id !== commentId
        ? comment
        : mapComments(comment)
    ))
  });

  const { posts: { expandedPost } } = getRootState();

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapPost(expandedPost)));
  }
};

export const deleteComment = ({ commentId, postId }) => async (dispatch, getRootState) => {
  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) - 1,
    comments: post.comments.filter(comment => comment.id !== commentId)
  });

  const comment = await commentService.deleteComment({ commentId, postId });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const applyComment = ({ postId, commentId }) => async (dispatch, getRootState) => {
  const { posts: { posts, expandedPost } } = getRootState();

  const mapCommentCount = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1
  });

  const updated = posts.map(post => (post.id !== postId
    ? post
    : mapCommentCount(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    const comment = await commentService.getComment(commentId);
    const mapComments = post => ({
      ...post,
      commentCount: Number(post.commentCount) + 1,
      comments: [...(post.comments || []), comment]
    });
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const refreshComment = ({ postId, commentId }) => async (dispatch, getRootState) => {
  const { posts: { expandedPost } } = getRootState();

  if (expandedPost && expandedPost.id === postId) {
    const updatedComment = await commentService.getComment(commentId);
    const mapComments = post => ({
      ...post,
      comments: post.comments.map(comment => (comment.id !== commentId ? comment : updatedComment))
    });

    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const removeComment = ({ postId, commentId }) => (dispatch, getRootState) => {
  const { posts: { posts, expandedPost } } = getRootState();

  const mapCommentCount = post => ({
    ...post,
    commentCount: Number(post.commentCount) - 1
  });

  const updated = posts.map(post => (post.id !== postId
    ? post
    : mapCommentCount(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    const mapComments = post => ({
      ...post,
      commentCount: Number(post.commentCount) + 1,
      comments: post.comments.filter(comment => comment.id !== commentId)
    });
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const refreshAllPost = ({ userId, username }) => async (dispatch, getRootState) => {
  const { posts: { posts, expandedPost } } = await getRootState();
  const updated = posts.map(post => (
    post.user.id !== userId
      ? post
      : { ...post, user: { ...post.user, username } }
  ));

  if (expandedPost) {
    const updatedExpandedPost = await postService.getPost(expandedPost.id);
    dispatch(setExpandedPostAction(updatedExpandedPost));
  }
  dispatch(setPostsAction(updated));
};

const showErrorAction = (errorMessage = '') => ({
  type: SHOW_ERROR,
  payload: errorMessage
});

export const showError = (errorMessage = '') => dispatch => {
  dispatch(showErrorAction(errorMessage));
  setTimeout(dispatch(showErrorAction('')), 4000);
};

export const likeComment = (postId, commentId, user, isLike) => async (dispatch, getRootState) => {
  const { diffDislike, commentReactions } = await commentService.likeComment(postId, commentId, user, isLike);

  const mapLikes = comment => ({
    ...comment,
    commentReactions,
    dislikeCount: Number(comment.dislikeCount) + diffDislike
  });

  const { posts: { expandedPost: { comments } } } = getRootState();
  const { posts: { expandedPost, posts } } = getRootState();
  const updatedComments = comments.map(comment => (comment.id !== commentId ? comment : mapLikes(comment)));
  const updatedPost = { ...expandedPost, comments: updatedComments };
  const updatedPosts = posts.map(post => (post.id !== postId ? post : updatedPost));

  dispatch(setPostsAction(updatedPosts));

  if (expandedPost) {
    dispatch(setExpandedPostAction(updatedPost));
  }
};
