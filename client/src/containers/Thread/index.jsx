import React, { useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Loader, Select } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import {
  loadPosts,
  loadMorePosts,
  likePost,
  toggleExpandedPost,
  addPost,
  showError
} from './actions';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  from: 0,
  count: 10
};

const Thread = ({
  userId,
  expandedPostId,
  hasMorePosts,
  posts = [],
  addPost: createPost,
  likePost: like,
  toggleExpandedPost: toggle,
  loadPosts: load,
  loadMorePosts: loadMore,
  showError: showErrorAction
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [getFilterValue, setFilterValue] = useState('showAllPosts');

  const getMorePosts = () => {
    loadMore(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const filterData = [
    { key: 'showAllPosts', value: 'showAllPosts', text: 'Show all posts', selected: true },
    { key: 'showOwnPosts', value: 'showOwnPosts', text: 'Show only my posts' },
    { key: 'showLikedMePosts', value: 'showLikedMePosts', text: 'Show posts liked me' },
    { key: 'hideOwnPosts', value: 'hideOwnPosts', text: 'Hide my posts' }
  ];

  const selectionHandler = (event, { value }) => {
    if (!value || value === getFilterValue) return;
    setFilterValue(value);
    if (value === 'showAllPosts') {
      postsFilter.filterValue = undefined;
      postsFilter.userId = undefined;
    } else {
      postsFilter.filterValue = value;
      postsFilter.userId = userId;
    }
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count;
  };

  const sharePost = id => {
    setSharedPostId(id);
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost
          addPost={createPost}
          uploadImage={uploadImage}
          showError={showErrorAction}
        />
      </div>
      <div className={styles.toolbar}>
        <Select
          placeholder="Show all posts"
          options={filterData}
          onChange={selectionHandler}
          defaultValue="showAllPosts"
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {posts.map(post => (
          <Post
            post={post}
            likePost={like}
            toggleExpandedPost={toggle}
            sharePost={sharePost}
            key={post.id}
            userId={userId}
            showError={showErrorAction}
          />
        ))}
      </InfiniteScroll>
      {expandedPostId && <ExpandedPost sharePost={sharePost} userId={userId} />}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId(undefined)}
          showError={showErrorAction}
        />
      )}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPostId: PropTypes.string,
  userId: PropTypes.string,
  loadPosts: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired,
  showError: PropTypes.func.isRequired
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPostId: undefined,
  userId: undefined
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState?.posts?.hasMorePosts,
  expandedPostId: rootState.posts?.expandedPost?.id,
  userId: rootState.profile.user.id,
  modal: rootState.posts.modal,
  deleteFunction: rootState.posts.deleteFunction,
  deleteData: rootState.posts.deleteData
});

const actions = {
  loadPosts,
  loadMorePosts,
  likePost,
  toggleExpandedPost,
  addPost,
  showError
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Thread);
