import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Label, Icon, Comment as CommentUI } from 'semantic-ui-react';
import moment from 'moment';

import UserLikeList from 'src/components/UserLikeList';

import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const Comment = ({
  comment: {
    body,
    createdAt,
    user,
    dislikeCount,
    id: commentId,
    commentReactions = []
  },
  userId,
  postId,
  updateComment,
  showError,
  modalConfirm,
  deleteComment,
  likeComment
}) => {
  const likeCount = commentReactions.filter(({ isLike }) => isLike).length;
  const [isEditComment, setIsEditComment] = useState(false);
  const [getBody, setBody] = useState(body);

  const handleCancelComment = async () => {
    await setBody(body);
    await setIsEditComment(false);
  };

  const handleUpdateComment = async () => {
    if (!getBody || getBody === body) {
      setBody(body);
      setIsEditComment(false);
      return;
    }
    try {
      await updateComment({
        commentId,
        postId,
        body: getBody
      });
    } catch (err) {
      setBody(body);
      showError('Comment update error.');
    } finally {
      setIsEditComment(false);
    }
  };

  const handleDeleteComment = async () => {
    try {
      await modalConfirm({
        deleteFunction: deleteComment,
        deleteData: { commentId, postId },
        backPath: '/',
        deleteMessage: 'Do you want to delete the comment?'
      });
    } catch (err) {
      showError('Comment deletion error.');
    }
  };

  const LikeComment = (
    <Label
      basic
      size="small"
      as="a"
      className={styles.toolbarBtn}
      onClick={() => likeComment(postId, commentId, userId, true)}
    >
      <Icon name="thumbs up" />
      {likeCount}
    </Label>
  );

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content className={styles.commentData}>
        <CommentUI.Author as="div" className={styles.authorData}>
          <div>{user.username}</div>
          <div>{user.status}</div>
        </CommentUI.Author>
        <CommentUI.Metadata className={styles.createdData}>
          {moment(createdAt).fromNow()}
        </CommentUI.Metadata>
        <CommentUI.Text
          onClick={
            userId === user.id
              ? () => setIsEditComment(true)
              : () => { }
          }
        >
          {isEditComment
            ? (
              <Form>
                <Form.TextArea
                  name="body"
                  value={getBody}
                  placeholder="What is the news?"
                  onChange={ev => setBody(ev.target.value)}
                />
              </Form>
            )
            : (
              body
            )}
        </CommentUI.Text>
        <CommentUI.Actions className={styles.actions}>
          <UserLikeList
            reactions={commentReactions}
            Like={LikeComment}
          />
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={() => likeComment(postId, commentId, userId, false)}
          >
            <Icon name="thumbs down" />
            {dislikeCount}
          </Label>
          {userId === user.id && (isEditComment
            ? (
              <Button.Group floated="right" size="mini">
                <Button color="teal" onClick={handleUpdateComment}>Save</Button>
                <Button.Or />
                <Button onClick={handleCancelComment}>Cancel</Button>
              </Button.Group>
            )
            : (
              <Button
                negative
                floated="right"
                size="mini"
                onClick={handleDeleteComment}
              >
                Delete
              </Button>
            ))}
        </CommentUI.Actions>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.shape({
    body: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
    dislikeCount: PropTypes.oneOfType([PropTypes.number.isRequired, PropTypes.string.isRequired]),
    id: PropTypes.string.isRequired,
    commentReactions: PropTypes.arrayOf(
      PropTypes.shape({
        isLike: PropTypes.bool.isRequired,
        user: PropTypes.shape({
          username: PropTypes.string.isRequired,
          id: PropTypes.string.isRequired
        })
      })
    ),
    user: PropTypes.shape({
      id: PropTypes.string.isRequired,
      username: PropTypes.string.isRequired,
      status: PropTypes.string,
      image: PropTypes.shape({
        id: PropTypes.string.isRequired,
        link: PropTypes.string
      })
    })
  }).isRequired,
  userId: PropTypes.string.isRequired,
  postId: PropTypes.string.isRequired,
  updateComment: PropTypes.func.isRequired,
  showError: PropTypes.func.isRequired,
  modalConfirm: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired
};

export default Comment;
