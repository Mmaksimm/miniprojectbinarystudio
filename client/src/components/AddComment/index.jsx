import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button } from 'semantic-ui-react';

const AddComment = ({
  postId,
  addComment,
  showError
}) => {
  const [body, setBody] = useState('');

  const handleAddComment = async () => {
    try {
      if (!body) {
        return;
      }
      await addComment({ postId, body });
      setBody('');
    } catch {
      showError('Error adding comment');
    }
  };

  return (
    <Form reply onSubmit={handleAddComment}>
      <Form.TextArea
        value={body}
        placeholder="Type a comment..."
        onChange={ev => setBody(ev.target.value)}
      />
      <Button type="submit" content="Post comment" labelPosition="left" icon="edit" primary />
    </Form>
  );
};

AddComment.propTypes = {
  addComment: PropTypes.func.isRequired,
  postId: PropTypes.string.isRequired,
  showError: PropTypes.func.isRequired
};

export default AddComment;
