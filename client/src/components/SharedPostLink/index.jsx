import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { Modal, Input, Icon, Button } from 'semantic-ui-react';
import * as postService from 'src/services/postService';

import styles from './styles.module.scss';

const SharedPostLink = ({ postId, close, showError }) => {
  const link = `${window.location.origin}/share/${postId}`;
  const email = encodeURI(`mailto:?subject=Link to post&body=${link}`);
  const [copied, setCopied] = useState(false);
  let input = useRef();

  const copyToClipboard = e => {
    try {
      input.select();
      document.execCommand('copy');
      e.target.focus();
      setCopied(true);
    } catch {
      showError('Error while copying to clipboard.');
    }
  };

  const handlerSharedByMail = async () => {
    try {
      await postService.sendMessageSharedByMail({ postId });
      window.location.replace(email);
    } catch {
      showError('Post share message was not sent.');
    }
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Share Post</span>
        {copied && (
          <span>
            <Icon color="green" name="copy" />
            Copied
          </span>
        )}
      </Modal.Header>
      <Modal.Content>
        <Input
          fluid
          action={{ labelPosition: 'right' }}
          value={link}
          ref={ref => { input = ref; }}
        >
          <input />
          <Button color="teal" onClick={copyToClipboard}>
            <Icon name="copy" />
          </Button>
          <Button
            color="teal"
            onClick={handlerSharedByMail}
          >
            <Icon name="envelope" />
          </Button>
        </Input>
      </Modal.Content>
    </Modal>
  );
};

SharedPostLink.propTypes = {
  postId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
  showError: PropTypes.func.isRequired
};

export default SharedPostLink;
