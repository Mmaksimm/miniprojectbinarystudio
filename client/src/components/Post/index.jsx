import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon, Form, Button } from 'semantic-ui-react';
import moment from 'moment';
import UserLikeList from 'src/components/UserLikeList';

import styles from './styles.module.scss';

const Post = ({
  post,
  likePost,
  toggleExpandedPost,
  sharePost,
  userId,
  canEdit,
  uploadImage,
  updateImage,
  deleteImage,
  updatePost,
  modalConfirm,
  showError,
  deletePost
}) => {
  const {
    id,
    image,
    body,
    user,
    dislikeCount,
    commentCount,
    createdAt,
    postReactions = []
  } = post;

  const likeCount = postReactions.filter(({ isLike }) => isLike === true).length;
  const date = moment(createdAt).fromNow();
  const [isEditPost, setIsEditPost] = useState(false);
  const [getImage, setImage] = useState(image);
  const [getBody, setBody] = useState(body);

  const handlePreviewImage = async ({ target }) => {
    try {
      const { files: [FileList] } = target;
      if (FileList.type.slice(0, 5) !== 'image') return;
      setImage({
        file: FileList,
        link: URL.createObjectURL(FileList)
      });
    } catch (err) {
      showError('Image preview error.');
    }
  };

  const handlePreviewDeleteImage = async () => {
    try {
      await modalConfirm({
        deleteFunction: setImage,
        deleteData: false,
        backPath: '/',
        deleteMessage: 'Do you want to delete the picture?'
      });
    } catch {
      showError('Image deletion error.');
    }
  };

  const handleUploadImage = async file => {
    const { link, id: imageId } = await uploadImage(file);
    setImage({ link, id: imageId });
    return {
      link,
      id: imageId
    };
  };

  const handleUpdateImage = async file => {
    const { link, id: imageId } = await updateImage({
      image: file,
      imageId: image.id
    });
    setImage({ link, id: imageId });
    return {
      link,
      id: imageId
    };
  };

  const handleDeleteImage = async () => {
    await deleteImage(image.id);
  };

  const handleCancelPost = () => {
    setBody(body);
    setImage(image);
    setIsEditPost(false);
  };

  const handleUpdatePost = async () => {
    if (!getBody || (getBody === body && getImage === image)) {
      setImage(image);
      setBody(body);
      setIsEditPost(false);
      return;
    }

    try {
      switch (getImage) {
        case image:
          await updatePost({
            postId: id,
            body: getBody
          });
          break;

        case false:
          await handleDeleteImage();
          await updatePost({
            postId: id,
            body: getBody,
            image: {
              id: null,
              link: null
            }
          });
          break;

        default:
          if (image && image.link) {
            const response = await handleUpdateImage(getImage.file);
            await updatePost({
              image: response,
              postId: id,
              body: getBody
            });
          } else {
            const response = await handleUploadImage(getImage.file);
            await updatePost({
              image: response,
              postId: id,
              body: getBody
            });
          }
      }
    } catch {
      setBody(body);
      setImage(image);
      showError('Post update error.');
    } finally {
      setIsEditPost(false);
    }
  };

  const handleDeletePost = async () => {
    try {
      await modalConfirm({
        deleteFunction: deletePost,
        deleteData: { postId: id },
        backPath: '/',
        deleteMessage: 'Do you want to delete the post?'
      });
    } catch (err) {
      showError('Post deletion error.');
    }
  };

  const LikePost = (
    <Label
      basic
      size="small"
      as="a"
      className={styles.toolbarBtn}
      onClick={() => likePost(id, user.id, true)}
    >
      <Icon name="thumbs up" />
      {likeCount}
    </Label>
  );

  return (
    <Card style={{ width: '100%' }}>
      {isEditPost
        ? getImage
        && (
          <Image
            src={getImage.link}
            wrapped
            ui={false}
          />
        )
        : image
        && (
          <Image
            src={image.link}
            wrapped
            ui={false}
            onClick={
              !canEdit
                ? () => toggleExpandedPost(id)
                : () => { }
            }
          />
        )}
      <Card.Content
        onClick={!canEdit
          ? () => toggleExpandedPost(id)
          : () => { }}
      >
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description
          onClick={
            canEdit && userId === user.id
              ? () => setIsEditPost(true)
              : () => { }
          }
        >
          {isEditPost
            ? (
              <Form onSubmit={handleUpdatePost}>
                <Form.TextArea
                  name="body"
                  value={getBody}
                  placeholder="What is the news?"
                  onChange={ev => setBody(ev.target.value)}
                  focus
                />
              </Form>
            )
            : (
              body
            )}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <UserLikeList
          reactions={postReactions}
          Like={LikePost}
        />
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => likePost(id, user.id, false)}
        >
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
        {isEditPost && !getImage && (
          <Button
            color="teal"
            icon
            labelPosition="left"
            as="label"
          >
            <Icon name="image" />
            Attach image
            <input name="image" type="file" onChange={handlePreviewImage} hidden />
          </Button>
        )}
        {isEditPost && getImage && (
          <Button.Group>
            <Button
              color="teal"
              icon
              labelPosition="left"
              as="label"
            >
              <Icon name="image" />
              Change Image
              <input name="image" type="file" onChange={handlePreviewImage} hidden />
            </Button>
            <Button.Or />
            <Button negative onClick={handlePreviewDeleteImage}>Delete Image</Button>
          </Button.Group>
        )}
        {canEdit && userId === user.id && (isEditPost
          ? (
            <Button.Group floated="right">
              <Button color="teal" onClick={handleUpdatePost}>Save</Button>
              <Button.Or />
              <Button onClick={handleCancelPost}>Cancel</Button>
            </Button.Group>
          )
          : <Button negative floated="right" onClick={handleDeletePost}>Delete</Button>)}
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
  canEdit: PropTypes.bool,
  uploadImage: PropTypes.func,
  updateImage: PropTypes.func,
  deleteImage: PropTypes.func,
  updatePost: PropTypes.func,
  modalConfirm: PropTypes.func,
  showError: PropTypes.func,
  deletePost: PropTypes.func
};

Post.defaultProps = {
  canEdit: false,
  uploadImage: () => { },
  updateImage: () => { },
  deleteImage: () => { },
  updatePost: () => { },
  showError: () => { },
  modalConfirm: () => { },
  deletePost: () => { }
};

export default Post;
