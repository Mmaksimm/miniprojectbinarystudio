import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { Header as HeaderUI, Image, Grid, Icon, Button, Input } from 'semantic-ui-react';

import styles from './styles.module.scss';

const Header = ({
  user,
  logout,
  updateProfile,
  showError,
  refreshAllPost
}) => {
  const { username, status, image, id } = user;
  const [isEdit, setEdit] = useState(false);
  const [getStatus, setStatus] = useState(status);

  const statusChanged = ({ target: { value } }) => {
    setStatus(value);
  };

  const handleCancel = () => {
    setStatus(status);
    setEdit(false);
  };

  const handleUpdateStatus = async () => {
    if (getStatus === status) {
      setStatus(status);
      return;
    }

    try {
      await updateProfile({
        userId: id,
        status: getStatus
      });
      await refreshAllPost({ username, userId: id, status: getStatus });
    } catch {
      showError('Status update error.');
    } finally {
      setStatus(status);
      setEdit(false);
    }
  };

  return (
    <div className={styles.headerWrp}>
      <Grid centered container columns="2">
        <Grid.Column>
          <HeaderUI>
            <NavLink exact to="/profile" className="ui header">
              <Image circular src={getUserImgLink(image)} />
            </NavLink>
            <HeaderUI.Content>
              <br />
              {username}
              <HeaderUI.Subheader>
                {isEdit
                  ? (
                    <>
                      {' '}
                      <Input
                        placeholder="Status"
                        type="text"
                        value={getStatus}
                        onChange={statusChanged}
                        className={styles.inputStatus}
                      />
                      <Button.Group className={styles.actionStatus}>
                        <Button
                          color="teal"
                          onClick={handleUpdateStatus}
                        >
                          Update
                        </Button>
                        <Button onClick={handleCancel}>Cancel</Button>
                      </Button.Group>
                    </>
                  )
                  : (
                    <span onClick={() => setEdit(true)}>
                      {' '}
                      {status}
                    </span>
                  )}
              </HeaderUI.Subheader>
            </HeaderUI.Content>
          </HeaderUI>
        </Grid.Column>
        <Grid.Column textAlign="right">
          <NavLink exact activeClassName="active" to="/profile" className={styles.menuBtn}>
            <Icon name="user circle" size="large" />
          </NavLink>
          <Button basic icon type="button" className={`${styles.menuBtn} ${styles.logoutBtn}`} onClick={logout}>
            <Icon name="log out" size="large" />
          </Button>
        </Grid.Column>
      </Grid>
    </div>
  );
};

Header.propTypes = {
  logout: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  updateProfile: PropTypes.func.isRequired,
  showError: PropTypes.func.isRequired,
  refreshAllPost: PropTypes.func.isRequired
};

export default Header;
