import React from 'react';
import PropTypes from 'prop-types';
import { Popup } from 'semantic-ui-react';

const UserLikeList = ({
  reactions,
  Like
}) => {
  const usersList = reactions.filter(postReaction => Boolean(postReaction.isLike))
    .sort(({ user: { username: usermane1 } }, { user: { username: usermane2 } }) => usermane1 > usermane2)
    .map(({ user: { username, id: key } }) => <li key={key}>{username}</li>);

  if (!usersList.length) {
    return (
      <Popup
        content={<ul>{usersList}</ul>}
        trigger={Like}
        disabled
      />
    );
  }

  return (
    <Popup
      content={<ul>{usersList}</ul>}
      trigger={Like}
      size="mini"
    />
  );
};

UserLikeList.propTypes = {
  reactions: PropTypes.arrayOf(PropTypes.object).isRequired,
  Like: PropTypes.object.isRequired // eslint-disable-line
};

export default UserLikeList;
