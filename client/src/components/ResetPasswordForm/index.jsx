import React, { useState } from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';

import { Form, Button, Segment } from 'semantic-ui-react';

const ResetPasswordForm = ({ resetPassword, showError }) => {
  const [email, setEmail] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isEmailValid, setIsEmailValid] = useState(true);

  const emailChanged = data => {
    setEmail(data.trim());
    setIsEmailValid(true);
  };

  const handleResetPasswordClick = async event => {
    event.preventDefault();
    if (!email || !isEmailValid || isLoading) {
      return;
    }
    setIsLoading(true);
    try {
      await resetPassword({ email });
    } catch {
      showError('Reset password error');
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Form name="resetPasswordForm" size="large" onSubmit={handleResetPasswordClick}>
      <Segment>
        <Form.Input
          fluid
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          error={!isEmailValid}
          onChange={ev => emailChanged(ev.target.value)}
          onBlur={() => setIsEmailValid(validator.isEmail(email))}
        />
        <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
          Send message
        </Button>
      </Segment>
    </Form>
  );
};

ResetPasswordForm.propTypes = {
  resetPassword: PropTypes.func.isRequired,
  showError: PropTypes.func.isRequired
};

export default ResetPasswordForm;
