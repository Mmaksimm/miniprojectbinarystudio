import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import PropTypes from 'prop-types';

import { Form, Button, Segment } from 'semantic-ui-react';

const UpdatePasswordForm = ({ updatePassword, showError }) => {
  const { token } = useParams();
  const [password, setPassword] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isPasswordValid, setIsPasswordValid] = useState(true);

  const passwordChanged = data => {
    setPassword(data);
    setIsPasswordValid(true);
  };

  const handleUpdatePasswordClick = async () => {
    if (!isPasswordValid || isLoading) {
      return;
    }
    setIsLoading(true);
    try {
      await updatePassword({ token, password });
    } catch {
      showError('Update password error.');
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Form name="updatePasswordForm" size="large" onSubmit={handleUpdatePasswordClick}>
      <Segment>
        <Form.Input
          fluid
          icon="lock"
          iconPosition="left"
          placeholder="Password"
          type="password"
          error={!isPasswordValid}
          onChange={ev => passwordChanged(ev.target.value)}
          onBlur={() => setIsPasswordValid(Boolean(password))}
        />
        <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
          Update Password
        </Button>
      </Segment>
    </Form>
  );
};

UpdatePasswordForm.propTypes = {
  updatePassword: PropTypes.func.isRequired,
  showError: PropTypes.func.isRequired
};

export default UpdatePasswordForm;
