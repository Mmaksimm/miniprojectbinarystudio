import callWebApi from 'src/helpers/webApiHelper';

export const updateProfile = async ({ userId, ...updatedProfile }) => {
  const response = await callWebApi({
    endpoint: `/api/users/${userId}`,
    type: 'PUT',
    request: updatedProfile
  });
  return response.json();
};
