import callWebApi from 'src/helpers/webApiHelper';

export const uploadImage = async image => {
  const response = await callWebApi({
    endpoint: '/api/images',
    type: 'POST',
    attachment: image
  });
  return response.json();
};

export const updateImage = async ({ imageId, image }) => {
  const response = await callWebApi({
    endpoint: `/api/images/${imageId}`,
    type: 'PUT',
    attachment: image
  });
  return response.json();
};

export const deleteImage = async imageId => {
  const response = await callWebApi({
    endpoint: `/api/images/${imageId}`,
    type: 'DELETE'
  });
  return response.json();
};
