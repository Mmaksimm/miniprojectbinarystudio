import callWebApi from 'src/helpers/webApiHelper';

export const addComment = async request => {
  const response = await callWebApi({
    endpoint: '/api/comments',
    type: 'POST',
    request
  });
  return response.json();
};

export const getComment = async id => {
  const response = await callWebApi({
    endpoint: `/api/comments/${id}`,
    type: 'GET'
  });
  return response.json();
};

export const updateComment = async ({ commentId, body }) => {
  const response = await callWebApi({
    endpoint: `/api/comments/${commentId}`,
    type: 'PUT',
    request: { body }
  });
  return response.json();
};

export const deleteComment = async ({ commentId, postId }) => {
  const response = await callWebApi({
    endpoint: `/api/comments/${commentId}`,
    type: 'DELETE',
    request: { postId }
  });
  return response.json();
};

export const likeComment = async (postId, commentId, user, isLike) => {
  const response = await callWebApi({
    endpoint: '/api/comments/react',
    type: 'PUT',
    request: {
      postId,
      commentId,
      user,
      isLike
    }
  });
  return response.json();
};
