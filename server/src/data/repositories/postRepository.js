/* eslint-disable default-case */
import { Op } from 'sequelize';
import sequelize from '../db/connection';
import {
  PostModel,
  CommentModel,
  UserModel,
  ImageModel,
  PostReactionModel,
  CommentReactionModel
} from '../models/index';
import BaseRepository from './baseRepository';

const likeCase = bool => `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class PostRepository extends BaseRepository {
  async getPosts({ id, filter }) {
    const {
      userId,
      filterValue,
      from: offset,
      count: limit
    } = filter;

    const where = {};
    const wherePostReactionFilter = {};

    switch (filterValue) {
      case 'showOwnPosts':
        Object.assign(where, { userId });
        break;

      case 'hideOwnPosts':
        Object.assign(where, { userId: { [Op.ne]: id } });
        break;

      case 'showLikedMePosts':
        Object.assign(where, { userId: { [Op.ne]: id } });
        Object.assign(wherePostReactionFilter, { where: { userId, isLike: true } });
        break;
    }

    return this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId")`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: PostReactionModel,
        attributes: ['isLike'],
        duplicating: false,
        ...wherePostReactionFilter,
        include: {
          model: UserModel,
          attributes: ['id', 'username']
        }
      }],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id',
        'postReactions.id',
        'postReactions->user.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
  }

  getPostById(id) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'comments->commentReactions.id',
        'comments->commentReactions->user.id',
        'user.id',
        'user->image.id',
        'image.id',
        'postReactions.id',
        'postReactions->user.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId")`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [
        {
          model: CommentModel,
          attributes: {
            include: [
              [sequelize
                .literal(`(SELECT COUNT(*)
                      FROM "commentReactions"
                      WHERE "comments"."id" = "commentReactions"."commentId"
                      AND "commentReactions"."isLike" = false)`), 'dislikeCount']
            ]
          },
          include: [
            {
              model: CommentReactionModel,
              attributes: ['isLike'],
              duplicating: false,
              include: {
                model: UserModel,
                attributes: ['id', 'username']
              }
            },
            {
              model: UserModel,
              attributes: [
                'id',
                'username',
                'status'
              ],
              include: {
                model: ImageModel,
                attributes: ['id', 'link']
              }
            }
          ]
        }, {
          model: UserModel,
          attributes: ['id', 'username', 'email'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        }, {
          model: ImageModel,
          attributes: ['id', 'link']
        }, {
          model: PostReactionModel,
          attributes: ['isLike'],
          duplicating: false,
          include: {
            model: UserModel,
            attributes: ['id', 'username']
          }
        }]
    });
  }
}

export default new PostRepository(PostModel);
