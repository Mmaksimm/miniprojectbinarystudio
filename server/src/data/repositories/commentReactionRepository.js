import { CommentReactionModel } from '../models';
import BaseRepository from './baseRepository';

class CommentReactionRepository extends BaseRepository {
  getCommentReaction(userId, commentId) {
    return this.model.findOne({
      attributes: ['id', 'isLike'],
      where: { userId, commentId }
    });
  }
}

export default new CommentReactionRepository(CommentReactionModel);
