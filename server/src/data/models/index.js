import orm from '../db/connection';
import associate from '../db/associations';

import initUserModel from './user';
import initPostModel from './post';
import initPostReactionModel from './postReaction';
import initCommentModel from './comment';
import initImageModel from './image';
import initCommentReactionModel from './commentReaction';

const UserModel = initUserModel(orm);
const PostModel = initPostModel(orm);
const PostReactionModel = initPostReactionModel(orm);
const CommentModel = initCommentModel(orm);
const ImageModel = initImageModel(orm);
const CommentReactionModel = initCommentReactionModel(orm);

associate({
  UserModel,
  PostModel,
  PostReactionModel,
  CommentModel,
  ImageModel,
  CommentReactionModel
});

export {
  UserModel,
  PostModel,
  PostReactionModel,
  CommentModel,
  ImageModel,
  CommentReactionModel
};
