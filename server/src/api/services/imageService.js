import axios from 'axios';
import { imgurId } from '../../config/imgurConfig';
import imageRepository from '../../data/repositories/imageRepository';

const uploadToImgur = async file => {
  try {
    const { data: { data } } = await axios.post(
      'https://api.imgur.com/3/image',
      {
        image: file.buffer.toString('base64')
      },
      {
        headers: { Authorization: `Client-ID ${imgurId}` }
      }
    );
    return {
      link: data.link,
      deleteHash: data.deletehash
    };
  } catch ({ response: { data: { status, data } } }) { // parse Imgur error
    return Promise.reject({ status, message: data.error });
  }
};

const deleteToImgur = async deleteHash => {
  try {
    const { data: { success } } = await axios.delete(
      `https://api.imgur.com/3/image/${deleteHash}`,
      {
        headers: { Authorization: `Client-ID ${imgurId}` }
      }
    );

    return success;
  } catch ({ response: { data: { status, data } } }) { // parse Imgur error
    return Promise.reject({ status, message: data.error });
  }
};

export const upload = async file => {
  const image = await uploadToImgur(file);
  return imageRepository.create(image);
};

export const deleteImage = async imageId => {
  const { deleteHash } = await imageRepository.getById(imageId);
  const success = await deleteToImgur(deleteHash);
  if (!success) throw Error('Picture heven\'t deleted');
  imageRepository.deleteById(imageId);
};

export const update = async (imageId, file) => {
  const { deleteHash } = await imageRepository.getById(imageId);
  await deleteToImgur(deleteHash);
  const image = await uploadToImgur(file);
  return imageRepository.updateById(imageId, image);
};
