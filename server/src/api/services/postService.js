/* eslint-disable no-nested-ternary */
import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';
import * as emailService from './emailService';
// For soft deletion, leave the pictures in the Imgur
// import * as imageService from './imageService';

export const getPosts = query => postRepository.getPosts(query);

export const getPostById = id => postRepository.getPostById(id);

export const create = async (userId, post) => {
  const { id } = await postRepository.create({
    ...post,
    userId
  });
  const newPost = await getPostById(id);
  const { user: { username } } = newPost;
  return {
    username,
    newPost,
    postId: id
  };
};

export const updatePost = async (postId, body) => {
  await postRepository.updateById(
    postId,
    body
  );
  const { userId, updatedAt, user: { username } } = await getPostById(postId);
  return {
    userId,
    postId,
    username,
    updatedAt
  };
};

export const deletePost = async postId => {
  const {
    user: {
      id: userId,
      username
    }
    // image = false
  } = await getPostById(postId);

  // For soft deletion, leave the pictures in the Imgur
  // if (image) await imageService.deleteImage(image.id);
  await postRepository.deleteById(postId);

  return {
    postId,
    userId,
    username
  };
};

export const setReaction = async (userId, { postId, isLike }, username) => {
  const reaction = await postReactionRepository.getPostReaction(userId, postId);
  const reactionIsLike = reaction
    ? reaction.toJSON().isLike
    : null;

  if (!reaction) {
    await postReactionRepository.create({ userId, postId, isLike });
  } else if ((isLike && reactionIsLike === true) || (!isLike && reactionIsLike === false)) {
    await postReactionRepository.deleteById(reaction.id);
  } else {
    await postReactionRepository.updateById(reaction.id, { isLike });
  }

  const diffLike = reactionIsLike === true
    ? -1
    : isLike
      ? 1
      : 0;

  const diffDislike = reactionIsLike === false
    ? -1
    : isLike
      ? 0
      : 1;

  const { postReactions, user: { email } } = await getPostById(postId);

  emailService.sendYourPostWasLikedMail({ username, postId, to: email });

  return { diffLike, diffDislike, postReactions };
};

export const sendYourPostWasSharedByMail = async ({ userId, username, postId }) => {
  const { user: { email, id } } = await getPostById(postId);
  if (id === userId) return;
  emailService.sendYourPostWasSharedByMail({ username, postId, to: email });
};
