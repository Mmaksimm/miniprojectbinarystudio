import userRepository from '../../data/repositories/userRepository';

export const getUserById = async userId => {
  const { id, username, email, image, status } = await userRepository.getUserById(userId);
  return { id, username, email, image, status };
};

export const updateUser = async (userId, body) => {
  await userRepository.updateById(userId, body);
  return getUserById(userId);
};
