import { createToken } from '../../helpers/tokenHelper';
import { encrypt } from '../../helpers/cryptoHelper';
import userRepository from '../../data/repositories/userRepository';
import { sendResetPasswordMail } from './emailService';

export const login = async ({ id }) => ({
  token: createToken({ id }),
  user: await userRepository.getUserById(id)
});

export const register = async ({ password, ...userData }) => {
  const newUser = await userRepository.addUser({
    ...userData,
    password: await encrypt(password)
  });
  return login(newUser);
};

export const resetPassword = async ({ email }) => {
  const { id } = await userRepository.getByEmail(email);

  await sendResetPasswordMail({
    to: email,
    token: createToken({ id })
  });
};

export const updatePassword = async ({ user: { id }, password }) => {
  await userRepository.updateById(
    id,
    { password: await encrypt(password) }
  );
  return login({ id });
};
