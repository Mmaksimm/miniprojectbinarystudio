/* eslint-disable no-nested-ternary */
import commentRepository from '../../data/repositories/commentRepository';
import commentReactionRepository from '../../data/repositories/commentReactionRepository';

export const getCommentById = id => commentRepository.getCommentById(id);

export const create = async (userId, comment) => {
  const { id } = await commentRepository.create({
    ...comment,
    userId
  });
  return getCommentById(id);
};

export const updateComment = async ({ commentId, body }) => commentRepository.updateById(commentId, { body });

export const deleteComment = commentId => commentRepository.deleteById(commentId);

export const setReaction = async (userId, { commentId, isLike }) => {
  const reaction = await commentReactionRepository.getCommentReaction(userId, commentId);
  const reactionIsLike = reaction
    ? reaction.toJSON().isLike
    : null;

  if (!reaction) {
    await commentReactionRepository.create({ userId, commentId, isLike });
  } else if ((isLike && reactionIsLike === true) || (!isLike && reactionIsLike === false)) {
    await commentReactionRepository.deleteById(reaction.id);
  } else {
    await commentReactionRepository.updateById(reaction.id, { isLike });
  }
  const diffLike = reactionIsLike === true
    ? -1
    : isLike
      ? 1
      : 0;

  const diffDislike = reactionIsLike === false
    ? -1
    : isLike
      ? 0
      : 1;

  const { commentReactions } = await getCommentById(commentId);

  return { diffLike, diffDislike, commentReactions };
};
