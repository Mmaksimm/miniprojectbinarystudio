import env from '../../env';
import { transporter } from '../../config/nodeMailerConfig';

const sendMail = async ({ to, subject, text }) => {
  const mailOptions = {
    from: `Thread <${env.nodemailer.email}>`,
    to,
    subject,
    html: `
      <h1>Thread</h1>
      <img src="http://s1.iconbird.com/ico/2013/8/428/w256h2561377930292cattied.png"/>
      <p>Hello&#8218;<p>
      <p>${text}</p>
      `
  };

  return new Promise((resolve, reject) => {
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        reject(error);
      } else {
        resolve(info);
      }
    });
  });
};

export const sendResetPasswordMail = async ({ to, token }) => {
  const message = {
    to,
    subject: 'Password reset instructions',
    text: `
      Please use the following <a href="${env.app.baseUrl}/update-password/${token}">link</a> to reset your password.
      `
  };
  await sendMail(message);
};

export const sendYourPostWasLikedMail = async ({ to, postId, username }) => {
  const message = {
    to,
    subject: 'Your post was liked',
    text: `
    ${username} liked your <a href="${env.app.baseUrl}/share/${postId}">post</a>.
      `
  };
  await sendMail(message);
};

export const sendYourPostWasSharedByMail = async ({ to, postId, username }) => {
  const message = {
    to,
    subject: 'Your Post Was Shared By Mail',
    text: `
    ${username} shared your <a href="${env.app.baseUrl}/share/${postId}">post</a>.
      `
  };
  await sendMail(message);
};

