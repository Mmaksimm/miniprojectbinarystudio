import { Router } from 'express';
import * as imageService from '../services/imageService';
import imageMiddleware from '../middlewares/imageMiddleware';

const router = Router();

router
  .post('/', imageMiddleware, (req, res, next) => imageService.upload(req.file)
    .then(image => res.send(image))
    .catch(next))
  .put('/:id', imageMiddleware, (req, res, next) => imageService.update(req.params.id, req.file)
    .then(image => res.send(image))
    .catch(next))
  .delete('/:id', (req, res, next) => imageService.deleteImage(req.params.id)
    .then(() => res.send({ success: true }))
    .catch(next));

export default router;
