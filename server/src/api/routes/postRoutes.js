import { Router } from 'express';
import * as postService from '../services/postService';

const router = Router();

router
  .get('/', (req, res, next) => postService.getPosts({ id: req.user.id, filter: req.query })
    .then(posts => res.send(posts))
    .catch(next))
  .get('/:id', (req, res, next) => postService.getPostById(req.params.id)
    .then(post => res.send(post))
    .catch(next))
  .post('/', (req, res, next) => postService.create(req.user.id, req.body)
    .then(({ postId, username, newPost }) => {
      req.io.emit('new_post', { username, postId, userId: req.user.id });
      return res.send(newPost);
    })
    .catch(next))
  .post('/shared', (req, res, next) => postService
    .sendYourPostWasSharedByMail({ userId: req.user.id, username: req.user.username, postId: req.body.postId })
    .then(() => { res.send({ success: true }); })
    .catch(next))
  .put('/react', (req, res, next) => postService.setReaction(req.user.id, req.body, req.user.username)
    .then(({ diffLike, diffDislike, postReactions }) => {
      const { postId } = req.body;
      req.io.emit('update_post', { postId, userId: req.user.id });
      if ((req.body.user !== req.user.id) && (diffLike === 1 || diffDislike === 1)) {
        // notify a user if someone (not himself) liked his post
        req.io.to(req.body.user)
          .emit('like-post', {
            postId,
            message: `Your post was ${diffDislike === 1 ? 'dis' : ''}liked!`
          });
      }
      return res.send({ diffLike, diffDislike, postReactions });
    })
    .catch(next))
  .put('/:id', (req, res, next) => postService.updatePost(req.params.id, req.body)
    .then(({ updatedAt, ...post }) => {
      req.io.emit('update_post', post);
      res.send({ updatedAt });
    })
    .catch(next))
  .delete('/:id', (req, res, next) => postService.deletePost(req.params.id)
    .then(response => {
      req.io.emit('delete_post', response);
      res.send({ success: true });
    })
    .catch(next));

export default router;
