import { Router } from 'express';
import * as userService from '../services/userService';

const router = Router();

router
  .get('/:id', (req, res, next) => userService.getuserById(req.params.id)
    .then(user => res.send(user))
    .catch(next))
  .put('/:id', (req, res, next) => userService.updateUser(req.params.id, req.body)
    .then(response => {
      req.io.emit('update_user', { userId: req.user.id, username: req.user.username });
      res.send(response);
    })
    .catch(next));

export default router;
