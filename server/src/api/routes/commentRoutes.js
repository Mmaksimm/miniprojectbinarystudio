import { Router } from 'express';
import * as commentService from '../services/commentService';

const router = Router();

router
  .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
    .then(comment => res.send(comment))
    .catch(next))
  .post('/', (req, res, next) => commentService.create(req.user.id, req.body)
    .then(comment => {
      req.io.emit('new_comment', { userId: req.user.id, postId: comment.postId, commentId: comment.id });
      res.send(comment);
    })
    .catch(next))
  .put('/react', (req, res, next) => commentService.setReaction(req.user.id, req.body)
    .then(({ diffDislike, commentReactions }) => {
      const { commentId, postId } = req.body;
      req.io.emit('like-comment', { commentId, postId, userId: req.user.id });
      return res.send({ diffDislike, commentReactions });
    })
    .catch(next))
  .put('/:id', (req, res, next) => commentService.updateComment({
    ...req.body,
    commentId: req.params.id
  })
    .then(({ updatedAt, postId }) => {
      req.io.emit('update_comment', { postId, userId: req.user.id, commentId: req.params.id });
      res.send({ updatedAt });
    })
    .catch(next))
  .delete('/:id', (req, res, next) => commentService.deleteComment(req.params.id)
    .then(() => {
      req.io.emit('delete_comment', { userId: req.user.id, postId: req.body.postId, commentId: req.params.id });
      res.send({ success: true });
    })
    .catch(next));

export default router;
