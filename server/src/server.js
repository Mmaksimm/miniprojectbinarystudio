import fs from 'fs';
import express from 'express';
import passport from 'passport';
import http from 'http';
import socketIO from 'socket.io';
import routes from './api/routes/index';
import authorizationMiddleware from './api/middlewares/authorizationMiddleware';
import errorHandlerMiddleware from './api/middlewares/errorHandlerMiddleware';
import routesWhiteList from './config/routesWhiteListConfig';
import socketInjector from './socket/injector';
import socketHandlers from './socket/handlers';
import sequelize from './data/db/connection';
import env from './env';
import './config/passportConfig';

const production = process?.env?.NODE_ENV === 'production' || false;

const app = express();
const server = http.createServer(app);
const io = socketIO(server);

const staticPath = production
  ? './build/client'
  : `${__dirname}/../../client/build`;

const port = production
  ? process.env.PORT
  : env.app.socketPort;

sequelize
  .authenticate()
  .then(() => {
    // eslint-disable-next-line no-console
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    // eslint-disable-next-line no-console
    console.error('Unable to connect to the database:', err);
  });

io.on('connection', socketHandlers);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(passport.initialize());

app.use(socketInjector(io));

app.use('/api/', authorizationMiddleware(routesWhiteList));

routes(app, io);

app.use((req, res, next) => {
  res.set({
    'Cache-Control': [
      'max-age=34164000',
      'public'
    ]
  });
  next();
});

app.use(express.static(staticPath));

app.get('*', (req, res) => {
  res.write(fs.readFileSync(`${staticPath}/index.html`));
  res.end();
});

app.use(errorHandlerMiddleware);

if (!production) {
  app.listen(env.app.port, () => {
    // eslint-disable-next-line no-console
    console.log(`Server listening on port ${env.app.port}!`);
  });
}

server.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Server listening on port ${env.app.port}!`);
});

